import { Component, OnInit } from '@angular/core';
import { FinalserviceService } from '../finalservice.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Route, Router } from '@angular/router';
// declare var jQuery: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  protected aFormGroup: any;
  registerForm!:FormGroup
  title='formvalidation';
  submitted=false;
  captchaResponse:any;
  countries: any;
  users:any;
  
  constructor( private router:Router,private service: FinalserviceService,private formBuilder: FormBuilder,private toastr: ToastrService) {
   this.users={name:'',mail:'',phoneNumber:'',gender:'',doj:'',country:'',password:''};

  }
  ngOnInit(){


    this.service.getAllCountries().subscribe((countriesData: any) => {
      this.countries = countriesData;
      console.log(countriesData);
    });
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    })

}
siteKey:string="6LdO7iwmAAAAAE9dcJCkvcSZXKCoCYGTE9-qe7uS";

 
  

// modalUser(){
//   console.log("kavya");
//   jQuery('#editEmployee').modal('show');
// }


register(regForm:any):any{
  
  console.log("HAi");
  
  console.log(regForm);
  // this.users.userId=regForm.userId;
  this.users.name=regForm.name;
  this.users.mail=regForm.mail;
  this.users.phoneNumber=regForm.phoneNumber;
  this.users.gender=regForm.gender;
  this.users.doj=regForm.doj;
  this.users.country=regForm.country;
  this.users.password=regForm.password;
  
console.log(this.users);
this.service.registerUserNew(this.users).subscribe((result:any)=>{
  console.log(result);
});

this.toastr.success("Successfully Registered ");
this.router.navigate(["login"]);

}

}

