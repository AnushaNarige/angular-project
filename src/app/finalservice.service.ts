import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FinalserviceService {
  userLogin(loginForm: any):any {
    return this.http.get('userLogin/' + loginForm.emailId + '/' + loginForm.password).toPromise();
  }
  constructor(private http: HttpClient) { 
  }
 
  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  registerUserNew(users:any):any{
    return this.http.post('/registerUsernew',users);
  }
}
